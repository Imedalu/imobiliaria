/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.controlador;

import br.edu.ifpb.ImplDAO.DAO;
import br.edu.ifpb.imobiliaria.Administrador;
import br.edu.ifpb.imobiliaria.Endereco;
import br.edu.ifpb.imobiliaria.Imobiliaria;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Denismark
 */
@Named("controladorImob")
@SessionScoped
public class ControladorImob implements Serializable {

    private String cnpj;

    @Inject
    private Imobiliaria imob;

    @Inject
    private Endereco endereco;

    @Inject
    private DAO dao;

    public Imobiliaria getImob() {
        return imob;
    }

    public void setImob(Imobiliaria imob) {
        this.imob = imob;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String salvar() {
        System.out.println("aaaaaaaaaaaaaaaaaa");
        imob.setEndereco(endereco);
        dao.save(imob);
        return "sucesso";
    }

    public String pesquisar() {
        imob = dao.pesquisaCnpjImob(cnpj);
        return "resultadopesquisaimob";
    }

    public String atualizar() {
        dao.update(imob);
        return "sucesso";
    }

    public String excluir() {
        dao.removePorId(Imobiliaria.class, imob.getId());
        return "sucesso";
    }

    public String cancelar() {
        return "index";
    }
}
