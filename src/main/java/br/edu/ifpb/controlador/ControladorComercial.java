package br.edu.ifpb.controlador;

import br.edu.ifpb.ImplDAO.DAO;
import br.edu.ifpb.imobiliaria.Endereco;
import br.edu.ifpb.imobiliaria.ImovComercial;
import br.edu.ifpb.imobiliaria.ImovResidencial;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Denismark
 */
@Named("controladorC")
@SessionScoped
public class ControladorComercial implements Serializable {

    private Integer codigo;

    @Inject
    private ImovComercial comercial;

    @Inject
    private Endereco endereco;

    @Inject
    private DAO dao;

    public ImovComercial getComercial() {
        return comercial;
    }

    public void setComercial(ImovComercial comercial) {
        this.comercial = comercial;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String salvar() {
        System.out.println("ccccccccccccc");
        comercial.setEndereco(endereco);
        dao.save(comercial);
        return "sucesso";
    }

    public String pesquisar() {
       comercial = dao.pesquisaImovComercial(codigo);
        System.out.println("dddddddd" + comercial.getTipo());
        return "resultadopesquisacomercial";
    }

    public String atualizar() {
        dao.update(comercial);
        return "sucesso";
    }

    public String excluir() {
        dao.removePorId(ImovComercial.class, comercial.getId());
        return "sucesso";
    }

    public String cancelar() {
        return "index";
    }

}
