/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.controlador;

import br.edu.ifpb.ImplDAO.DAO;
import br.edu.ifpb.imobiliaria.Endereco;
import br.edu.ifpb.imobiliaria.ImovResidencial;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Denismark
 */
@Named("controladorR")
@SessionScoped
public class ControladorResidencial implements Serializable{
    
    private Integer codigo;

    @Inject
    private ImovResidencial residencia;

    @Inject
    private Endereco endereco;

    @Inject
    private DAO dao;

    public ImovResidencial getResidencia() {
        return residencia;
    }

    public void setResidencia(ImovResidencial residencia) {
        this.residencia = residencia;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    
    public String salvar() {
        residencia.setEndereco(endereco);
        dao.save(residencia);
        return "sucesso";
    }

     public String pesquisar() {
        residencia = dao.pesquisaImovResid(codigo);
         System.out.println("dddddddd"+ residencia.getTipo());
        return "resultadopesquisaresid";
    }

    public String atualizar() {
        dao.update(residencia);
        return "sucesso";
    }

    public String excluir() {
        dao.removePorId(ImovResidencial.class, residencia.getId());
        return "sucesso";
    }
    public String cancelar() {
        return "index";
    }

}
