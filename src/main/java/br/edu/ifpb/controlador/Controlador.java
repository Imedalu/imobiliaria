package br.edu.ifpb.controlador;

import br.edu.ifpb.ImplDAO.DAO;
import br.edu.ifpb.imobiliaria.Cliente;
import br.edu.ifpb.imobiliaria.Endereco;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Denismark
 */
@Named("controlador")
@SessionScoped
public class Controlador implements Serializable{
    private String cpf;

    @Inject
    private Cliente cliente;

    @Inject
    private Endereco endereco;
    
    @Inject
    private DAO dao;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
        
    public String salvar() {
        System.out.println("aaaaaaaaaaaaaaaaaa");
        cliente.setEndereco(endereco);
        dao.save(cliente);
        cliente = new Cliente();
        return "sucesso";
    }

    public String pesquisar(){
       cliente = dao.pesquisaCpf(cpf);
       return "resultadopesquisa";
     }
    public String atualizar(){
        dao.update(cliente);
        return "sucesso";
    }
    
    public String excluir(){
     dao.removePorId(Cliente.class,cliente.getId());
     return "sucesso";
    }
    
    public String cancelar() {
        return "index";
    }

}
