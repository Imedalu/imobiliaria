/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifpb.controlador;

import br.edu.ifpb.ImplDAO.DAO;
import br.edu.ifpb.imobiliaria.Administrador;
import br.edu.ifpb.imobiliaria.Cliente;
import br.edu.ifpb.imobiliaria.Endereco;
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Denismark
 */
@Named("controladorAdmin")
@SessionScoped
public class ControladorAdmin implements Serializable{
    private String cpf;

    @Inject
    private Administrador admin;

    @Inject
    private Endereco endereco;

    @Inject
    private DAO dao;

    public Administrador getAdmin() {
        return admin;
    }

    public void setAdmin(Administrador admin) {
        this.admin = admin;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String salvar() {
        System.out.println("aaaaaaaaaaaaaaaaaa");
        admin.setEndereco(endereco);
        dao.save(admin);
        return "sucesso";
    }

    public String pesquisar() {
        admin = dao.pesquisaCpfAdmin(cpf);
        return "resultadopesquisaadmin";
    }

    public String atualizar() {
        dao.update(admin);
        return "sucesso";
    }

    public String excluir(){
     dao.removePorId(Administrador.class,admin.getId());
     return "sucesso";
    }
    public String cancelar() {
        return "index";
    }

}
