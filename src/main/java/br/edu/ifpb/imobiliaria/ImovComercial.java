package br.edu.ifpb.imobiliaria;

import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Dependent
public class ImovComercial implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    private String tipo;
    private Integer codigo;
    private Double qtde_metros_quadrados;
    private Integer qtde_banheiro;
    private Integer qtde_escritorio;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ENDERECO_ID")
    private Endereco endereco;

    public ImovComercial(int id, Endereco endereco, String tipo, Integer codigo, Double qtde_metros_quadrados, Integer qtde_banheiro, Integer qtde_escritorio) {
        this.id = id;
        this.endereco = endereco;
        this.qtde_metros_quadrados = qtde_metros_quadrados;
        this.qtde_banheiro = qtde_banheiro;
        this.qtde_escritorio = qtde_escritorio;
        this.codigo = codigo;
        this.tipo = tipo;

    }

    public ImovComercial() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Double getQtde_metros_quadrados() {
        return qtde_metros_quadrados;
    }

    public void setQtde_metros_quadrados(Double qtde_metros_quadrados) {
        this.qtde_metros_quadrados = qtde_metros_quadrados;
    }

    public Integer getQtde_banheiro() {
        return qtde_banheiro;
    }

    public void setQtde_banheiro(Integer qtde_banheiro) {
        this.qtde_banheiro = qtde_banheiro;
    }

    public Integer getQtde_escritorio() {
        return qtde_escritorio;
    }

    public void setQtde_escritorio(Integer qtde_escritorio) {
        this.qtde_escritorio = qtde_escritorio;
    }

    @Override
    public String toString() {
        return "ImovComercial{" + "id=" + id + ", tipo=" + tipo + ", codigo=" + codigo + ", qtde_metros_quadrados=" + qtde_metros_quadrados + ", qtde_banheiro=" + qtde_banheiro + ", qtde_escritorio=" + qtde_escritorio + ", endereco=" + endereco + '}';
    }

}
