package br.edu.ifpb.imobiliaria;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Reserva implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    @Temporal(TemporalType.DATE)
    private Date data_reserva;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IMOVCOMERCIAL_ID")
    private ImovComercial imovelComercial;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IMOVRESIDENCIAL_ID")
    private ImovResidencial imovelResidencial;

    public Reserva(int id, ImovComercial imovComercial, ImovResidencial imovResidencial, Date data_reserva) {
        this.id = id;
        this.imovelComercial = imovComercial;
        this.imovelResidencial = imovResidencial;
        this.data_reserva = data_reserva;
    }

    public Reserva() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getData_reserva() {
        return data_reserva;
    }

    public void setData_reserva(Date data_reserva) {
        this.data_reserva = data_reserva;
    }

    public ImovComercial getImovelComercial() {
        return imovelComercial;
    }

    public void setImovelComercial(ImovComercial imovelComercial) {
        this.imovelComercial = imovelComercial;
    }

    public ImovResidencial getImovelResidencial() {
        return imovelResidencial;
    }

    public void setImovelResidencial(ImovResidencial imovelResidencial) {
        this.imovelResidencial = imovelResidencial;
    }

    @Override
    public String toString() {
        return "Reserva{" + "id=" + id + ", data_reserva=" + data_reserva + ", imovelComercial=" + imovelComercial + ", imovelResidencial=" + imovelResidencial + '}';
    }

}
