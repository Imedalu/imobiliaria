package br.edu.ifpb.imobiliaria;

import java.io.Serializable;
import java.util.List;
import javax.enterprise.context.Dependent;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@Dependent
public class Imobiliaria implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    private String nome;
    private String cnpj;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADMINISTRADOR_ID")
    private Administrador Administrador;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Cliente> clientes;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ImovComercial> comercios;

    private List<ImovResidencial> residencias;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ENDERECO_ID")
    private Endereco endereco;

    public Imobiliaria(int id, String nome, String cnpj, Endereco endereco, Administrador administrador, List<Cliente> clientes,
            List<ImovComercial> comercios, List<ImovResidencial> residencias) {
        super();
        this.id = id;
        this.nome = nome;
        this.cnpj = cnpj;
        this.endereco = endereco;
        this.Administrador = administrador;
        this.clientes = clientes;
        this.comercios = comercios;
        this.residencias = residencias;
    }

    public Imobiliaria() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Administrador getAdministrador() {
        return Administrador;
    }

    public void setAdministrador(Administrador Administrador) {
        this.Administrador = Administrador;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public List<ImovComercial> getComercios() {
        return comercios;
    }

    public void setComercios(List<ImovComercial> comercios) {
        this.comercios = comercios;
    }

    public List<ImovResidencial> getResidencias() {
        return residencias;
    }

    public void setResidencias(List<ImovResidencial> residencias) {
        this.residencias = residencias;
    }

    @Override
    public String toString() {
        return "Imobiliaria{" + "id=" + id + ", nome=" + nome + ", cnpj=" + cnpj + ", Administrador=" + Administrador + ", clientes=" + clientes + ", comercios=" + comercios + ", residencias=" + residencias + ", endereco=" + endereco + '}';
    }

}
