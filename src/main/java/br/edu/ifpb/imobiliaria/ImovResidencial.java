package br.edu.ifpb.imobiliaria;

import java.io.Serializable;
import javax.enterprise.context.Dependent;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
@Dependent
public class ImovResidencial implements Serializable {

    @Id
    @GeneratedValue
    private int id;
    private String tipo;
    private Integer codigo;
    private int qtdeQuarto;
    private int qtdeSala;
    private int qtdeBanheiro;
    private int qtdeGaragem;
    private double qtdeMetrosQuadrados;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ENDERECO_ID")
    private Endereco endereco;

    public ImovResidencial(int id, Endereco endereco,String tipo, Integer codigo, int qtdeQuarto, int qtdeSala, int qtdeBanheiro, int qtdeGaragem, double qtdeMetrosQuadrados) {
        this.id = id;
        this.endereco = endereco;
        this.qtdeQuarto = qtdeQuarto;
        this.qtdeSala = qtdeSala;
        this.qtdeBanheiro = qtdeBanheiro;
        this.qtdeGaragem = qtdeGaragem;
        this.qtdeMetrosQuadrados = qtdeMetrosQuadrados;
        this.codigo = codigo;
        this.tipo = tipo;
    }

    public ImovResidencial() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getQtdeQuarto() {
        return qtdeQuarto;
    }

    public void setQtdeQuarto(int qtdeQuarto) {
        this.qtdeQuarto = qtdeQuarto;
    }

    public int getQtdeSala() {
        return qtdeSala;
    }

    public void setQtdeSala(int qtdeSala) {
        this.qtdeSala = qtdeSala;
    }

    public int getQtdeBanheiro() {
        return qtdeBanheiro;
    }

    public void setQtdeBanheiro(int qtdeBanheiro) {
        this.qtdeBanheiro = qtdeBanheiro;
    }

    public int getQtdeGaragem() {
        return qtdeGaragem;
    }

    public void setQtdeGaragem(int qtdeGaragem) {
        this.qtdeGaragem = qtdeGaragem;
    }

    public double getQtdeMetrosQuadrados() {
        return qtdeMetrosQuadrados;
    }

    public void setQtdeMetrosQuadrados(double qtdeMetrosQuadrados) {
        this.qtdeMetrosQuadrados = qtdeMetrosQuadrados;
    }

    @Override
    public String toString() {
        return "ImovResidencial{" + "id=" + id + ", tipo=" + tipo + ", codigo=" + codigo + ", qtdeQuarto=" + qtdeQuarto + ", qtdeSala=" + qtdeSala + ", qtdeBanheiro=" + qtdeBanheiro + ", qtdeGaragem=" + qtdeGaragem + ", qtdeMetrosQuadrados=" + qtdeMetrosQuadrados + ", endereco=" + endereco + '}';
    }

    
  }
