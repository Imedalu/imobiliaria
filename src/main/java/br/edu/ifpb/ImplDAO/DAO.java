package br.edu.ifpb.ImplDAO;

import br.edu.ifpb.imobiliaria.Administrador;
import br.edu.ifpb.imobiliaria.Cliente;
import br.edu.ifpb.imobiliaria.Imobiliaria;
import br.edu.ifpb.imobiliaria.ImovComercial;
import br.edu.ifpb.imobiliaria.ImovResidencial;


/**
 *
 * @author Denismark
 */
public interface DAO {
    public void save(Object object);
    public Object find(Class classe, Object object);
    public void update(Object object);
    public void delete(Object object);   
    public Object localizarPorId(Class classe, Object id);
    public void removePorId(Class classe, Object id);
    public Cliente pesquisaCpf(String cpf);
    public Administrador pesquisaCpfAdmin(String cpf);
    public Imobiliaria pesquisaCnpjImob(String cnpj);
    public ImovResidencial pesquisaImovResid(Integer codigo);
    public ImovComercial pesquisaImovComercial(Integer codigo);
    
}
