package br.edu.ifpb.ImplDAO;

import br.edu.ifpb.imobiliaria.Administrador;
import br.edu.ifpb.imobiliaria.Cliente;
import br.edu.ifpb.imobiliaria.Imobiliaria;
import br.edu.ifpb.imobiliaria.ImovComercial;
import br.edu.ifpb.imobiliaria.ImovResidencial;
import java.io.Serializable;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Denismark
 */
@Stateful
@RequestScoped
public class DAOJPA implements DAO, Serializable {

    @PersistenceContext(unitName = "imobPU")
    private EntityManager em;

    public DAOJPA() {

    }

    @Override
    public void save(Object object) {
        em.persist(object);
    }

    @Override
    public Object find(Class classe, Object object) {
        return em.find(classe, object);
    }

    @Override
    public void update(Object object) {
        em.merge(object);
    }

    @Override
    public void delete(Object object) {
        em.remove(object);
    }
    @Override
     public Object localizarPorId(Class classe, Object id) {
        return em.find(classe, id);
    }
    @Override
    public void removePorId(Class classe, Object id) {
        delete(localizarPorId(classe, id));       
    }

    @Override
    public Cliente pesquisaCpf(String cpf) {
        Cliente cliente = new Cliente();
        String jpql = "select c from Cliente c where c.cpf =:cpf";
        Query query = em.createQuery(jpql);
        query.setParameter("cpf", cpf);
        cliente = (Cliente) query.getSingleResult();
        return cliente;
    }
    
    @Override
    public Administrador pesquisaCpfAdmin(String cpf) {
        Administrador admin = new Administrador();
        String jpql = "select a from Administrador a where a.cpf =:cpf";
        Query query = em.createQuery(jpql);
        query.setParameter("cpf", cpf);
        admin = (Administrador) query.getSingleResult();
        return admin;
    }
    @Override
    public Imobiliaria pesquisaCnpjImob(String cnpj) {
        Imobiliaria imob = new Imobiliaria();
        String jpql = "select i from Imobiliaria i where i.cnpj =:cnpj";
        Query query = em.createQuery(jpql);
        query.setParameter("cnpj", cnpj);
        imob = (Imobiliaria) query.getSingleResult();
        return imob;
    }

    @Override
    public ImovResidencial pesquisaImovResid(Integer codigo) {
        ImovResidencial imovResid = new ImovResidencial();
        String jpql = "select i from ImovResidencial i where i.codigo =:codigo";
        Query query = em.createQuery(jpql);
        query.setParameter("codigo", codigo);
        imovResid = (ImovResidencial) query.getSingleResult();
        return imovResid;
    }

    @Override
    public ImovComercial pesquisaImovComercial(Integer codigo) {
         ImovComercial imovComercial = new ImovComercial();
        String jpql = "select ic from ImovComercial ic where ic.codigo =:codigo";
        Query query = em.createQuery(jpql);
        query.setParameter("codigo", codigo);
        imovComercial = (ImovComercial) query.getSingleResult();
        return imovComercial;
        
    }

}
